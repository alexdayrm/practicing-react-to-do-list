import React, {useState} from 'react'
import './App.css'

export default function App() {

  const [newItem, setNewItem] = useState("")
  const [list, setList] = useState([])

  // adds user input to newItem state as they type it
  const handleChange = (e) => {
    setNewItem(e.target.value)
  }
    
  // adds item to list in state
  const addItem = () => {

    const item = {
      id: list.length + 1,
      value: newItem
    }
    
    setList(list.concat(item))
    // resets newItem state ready for next input
    setNewItem("")
  }

  // checks if enter/return key has been pressed in the input box
  const returnCheck = (e) => {
    if (e.charCode === 13) {
    addItem()
    }
  }

  // deletes item from the list in state
  const deleteItem = (id) => {
    const updatedList = list.filter(item => item.id !== id)
    setList(updatedList)
  }

  return (
    <div className="App">
      <div>
        Add an item
        <br />
        <input
          type="text"
          placeholder="Type item here"
          value={newItem}
          onChange={handleChange}
          onKeyPress={returnCheck}
        />
        <button onClick={addItem}>
          Add
        </button>
        <br/>
        <ul>
          {list.map(item => {
            return(
              <li key={item.id}>{item.value}
                <button onClick={() => deleteItem(item.id)}>
                X
                </button>
              </li>
            )
          })}
        </ul>
      </div>
    </div>
  )
}
